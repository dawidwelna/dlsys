import os
import pandas as pd
from dlsys.neuralnet import NeuralNet
from dlsys.autoencoder import AutoEncoder
from flask import Flask, render_template, session, redirect, \
    url_for, flash, request, Blueprint, g
from DLsystem.deep.forms import NameForm, ParameterForm, UploadForm, \
    ResultForm, SimpleUpload, AutoEncoderForm, EncoderResultForm
from werkzeug.utils import secure_filename
from wtforms.validators import InputRequired
from wtforms import SelectField
from DLsystem.utils import flash_errors


blueprint = Blueprint('deep', __name__, url_prefix='/', static_folder='../static')

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def has_column_name(column_values):
    are_numbers = [True for val in column_values if is_number(val)]
    if True in are_numbers:
        return False
    return True


def load_dataset(destination, index_col, header, ext):
    """

    :param destination: path to folder where the file is located
    :param index_col: the integer: most likely 0 or -1
    :param header: the integer. Eg. 0 indicating that the first row is header
    :param ext: extension that is the format of the file .csv or .xlsx
    :return: loaded pandas dataframe
    """
    if ext == 'csv':
        data_set = pd.read_csv(destination, index_col=index_col, header=header)
    elif ext == 'excel':
        data_set = pd.read_excel(destination, index_col=index_col, header=header)

    return data_set


@blueprint.route('/mlp_net', methods=['GET', 'POST'])
def mlp_net():
    try:
        global data_set
        # check if the indexed column shall be None or integer value.
        if session['index_col'] != "None":
            index_col = int(session['index_col'])
        else:
            index_col = None
        if session['header'] != "None":
            header = int(session['header'])
        else:
            header = None
        data_set = load_dataset(destination=session['destination'],
                                index_col=index_col, header=header, ext=session['extension'])

        class NodesInLayers(ParameterForm):
            pass

        for i in range(0, session['n_layers']):
            if i == 0:
                default = data_set.shape[1] - 1
            elif i == session['n_layers'] - 1:
                default = 3
                #     default = len(data_set[target_col].unique())
            else:
                default = 16

            setattr(NodesInLayers, 'layer{}'.format(i + 1),
                    SelectField('Nodes in layer {}'.format(i + 1),
                                choices=[(el, el) for el in range(0, 50)], validators=[InputRequired()], coerce=int,
                                default=default)
                    )
        form = NodesInLayers()

        if has_column_name(data_set.columns.values):
            column_names = [(el, el) for el in data_set.columns]
        else:
            column_names = [(str(el), str(el)) for el in data_set.columns]

        form.target_col.choices = column_names
        global net
        if form.validate_on_submit():
            layers_nodes = [getattr(form, 'layer{}'.format(i + 1)).data for i in range(0, session['n_layers'])]
            if is_number(form.target_col.data):
                target_col = int(form.target_col.data)
            else:
                target_col = form.target_col.data
            net = NeuralNet(data_set=data_set, target_col=target_col)
            net.train_test_shuffle(form.test_size.data, seed=1234)
            net.configure_net(layers_nodes, activ_func=form.activation_func.data)
            # net_train = net.train(form.alpha.data, form.epochs.data)
            # net_test = net.test_model()
            return redirect(url_for('deep.result', alpha=form.alpha.data, epochs=form.epochs.data))
        return render_template('deep/mlp_net.html', dataset=data_set.to_html(), form=form, n_layers=session['n_layers'])
    except UnboundLocalError:
        return redirect(url_for('public.home'))


@blueprint.route('/result/alpha=<alpha>epochs=<epochs>', methods=['GET', 'POST'])
def result(alpha, epochs):
    form = ResultForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            epochs = form.epochs.data
            alpha = form.alpha.data

            net_train = net.train(alpha, epochs)
            net_test = net.test_model()
            flash("The model has been trained again.")
            render_template('deep/result.html', form=form, net_train=net_train, net_test=net_test)
        else:
            flash_errors(form)

    net_train = net.train(float(alpha), int(epochs))
    net_test = net.test_model()

    return render_template('deep/result.html', form=form, net_train=net_train, net_test=net_test)


@blueprint.route('/result_encoder?epochs=<epochs>?test_size=<test_size>?factor=<encode_factor>?target=<target_col>',
                 methods=['GET', 'POST'])
def result_encoder(epochs, test_size, encode_factor, target_col):
    encoder = AutoEncoder(data_set, int(test_size), int(encode_factor), target_col)
    encoder.compile()
    form = EncoderResultForm()

    encoder.fit(nb_epoch=int(epochs))
    loss_history = encoder.get_history_callback()
    summary = encoder.get_summary()
    df = encoder.get_encoded_df()

    if request.method == 'POST':
        if form.validate_on_submit():
            if form.save_file.data:
                target = os.path.join(basedir, 'dlsys/data/')
                df.to_csv(target+form.file.data+'.csv')
                flash("The file has been saved", 'success')

            if form.submit.data:
                epochs = form.epochs.data
                encoder.fit(nb_epoch=form.epochs.data)
                flash("The model has been fit again.")

            return redirect(url_for('deep.result_encoder', epochs=epochs,
                                    test_size=test_size, encode_factor=encode_factor,
                                    target_col=target_col))
        else:
            flash_errors(form)

    return render_template('deep/result_encoder.html', form=form, loss_history=loss_history,
                           summary=summary, df=df.to_html())


@blueprint.route('/autoencoder', methods=['GET', 'POST'])
def autoencoder():
    # check if the indexed column shall be None or integer value.
    global data_set
    if session['index_col'] != "None":
        index_col = int(session['index_col'])
    else:
        index_col = None
    if session['header'] != "None":
        header = int(session['header'])
    else:
        header = None
    data_set = load_dataset(destination=session['destination'],
                            index_col=index_col, header=header, ext=session['extension'])

    form = AutoEncoderForm()

    if has_column_name(data_set.columns.values):
        column_names = [(el, el) for el in data_set.columns]
    else:
        column_names = [(str(el), str(el)) for el in data_set.columns]

    form.target_col.choices = column_names

    if form.validate_on_submit():
        return redirect(url_for('deep.result_encoder', epochs=form.epochs.data, test_size=
                                form.test_size.data, encode_factor=form.encode_factor.data,
                                target_col=form.target_col.data))

    return render_template('deep/autoencoder.html', form=form, dataset=data_set.to_html())


@blueprint.route('/simple_upload', methods=['GET', 'POST'])
def simple_upload():
    form = SimpleUpload()
    target = os.path.join(basedir, 'dlsys/data')
    if not os.path.isdir(target):
        os.mkdir(target)

    if form.validate_on_submit():
        print(form.file.data.filename)
        filename = secure_filename(form.file.data.filename)
        destination = "/".join([target, filename])
        print(destination)
        form.file.data.save(destination)

        session['extension'] = form.extension.data
        session['destination'] = destination
        session['index_col'] = form.index_col.data
        session['header'] = form.header.data
        return redirect(url_for('deep.autoencoder'))
    return render_template('deep/simple_upload.html', form=form)


@blueprint.route('/upload', methods=['GET', 'POST'])
def upload():
    form = UploadForm()
    target = os.path.join(basedir, 'dlsys/data')

    if not os.path.isdir(target):
        os.mkdir(target)

    if form.validate_on_submit():
        print(form.file.data.filename)
        filename = secure_filename(form.file.data.filename)
        destination = "/".join([target, filename])
        print(destination)
        form.file.data.save(destination)

        session['extension'] = form.extension.data
        session['destination'] = destination
        session['index_col'] = form.index_col.data
        session['header'] = form.header.data
        session['n_layers'] = form.n_layers.data
        return redirect(url_for('deep.mlp_net'))
    return render_template('deep/upload.html', form=form)

# target_col <-- buttion choice
# test_size <-- percentage button
# net config <-- 4 pola
# activation function <-- button choice
# alpha <--- rozwijany
# epochs <-- rozwijany
