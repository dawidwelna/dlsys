import numpy as np
from flask_wtf import FlaskForm, Form
from wtforms.validators import DataRequired, InputRequired, required
from wtforms import StringField, SubmitField, SelectField, FieldList
from flask_wtf.file import FileField
from flask_wtf.file import FileRequired


class EncoderResultForm(FlaskForm):
    choice_epochs = [(el, el) for el in range(100, 3100, 100)]
    epochs = SelectField('Epochs', choices=choice_epochs, validators=[InputRequired()], coerce=int)
    submit = SubmitField('Train again..')
    file = StringField(label='Filename', default='encoded_dataset')
    save_file = SubmitField(label='Save file')


class ResultForm(FlaskForm):
    choice_epochs = [(el, el) for el in range(100, 3100, 100)]
    epochs = SelectField('Epochs', choices=choice_epochs, validators=[InputRequired()], coerce=int)

    choice_alpha = [(el, "{0:.3f}".format(el)) for el in np.arange(0.000, 1.0, 0.003)]
    choice_alpha[0] = (0.001, '0.001')
    alpha = SelectField('Learning rate', choices=choice_alpha,
                        validators=[InputRequired()], coerce=float)
    submit = SubmitField('Train again..')


class NameForm(FlaskForm):
    name = StringField('What is your name?', validators=[DataRequired()])
    submit = SubmitField('Submit')


class LayerForm(FlaskForm):
    nodes_number = SelectField('Choose number of nodes',
                               choices=[(el, el) for el in range(0, 50)], validators=[InputRequired()], coerce=int)


class AutoEncoderForm(FlaskForm):
    target_col = SelectField('Target column(class identifier)', choices=[], validators=[InputRequired()], coerce=str)

    choice_epochs = [(el, el) for el in range(100, 3100, 100)]
    choice_test = [(el, el) for el in range(0, 101)]
    test_size = SelectField('Test set [%]', choices=choice_test, validators=[InputRequired()], coerce=int)
    epochs = SelectField('Epochs', choices=choice_epochs, validators=[InputRequired()], coerce=int)
    choice_factor = [(el, el) for el in range(2, 16, 2)]
    encode_factor = SelectField('Compression factor', choices=choice_factor, validators=[InputRequired()], coerce=int)

    submit = SubmitField('Submit')


class ParameterForm(FlaskForm):
    target_col = SelectField('Target column(class identifier)', choices=[], validators=[InputRequired()], coerce=str)

    choice_test = [(el, el) for el in range(0, 101)]
    test_size = SelectField('Test set [%]', choices=choice_test, validators=[InputRequired()], coerce=int)

    # net_config = StringField('Nodes in layers', validators=[InputRequired()])
    # n_layers = SelectField('Number of layers', choices=[(el, el) for el in range(0, 10)],
    #                        validators=[InputRequired()], coerce=int)
    #
    # layers = FieldList(FormField(LayerForm))
    # nodes_in_layer = []
    # for i in n_layers:
    #     nodes_in_layer[i] = SelectField('Nodes in layer {}'.format(i),
    #                                     choice=[(el, el) for el in range(0, 50)], validators=[InputRequired()])

    activation_func = SelectField('Activation function',
                                  choices=[('Sigmoid', 'Sigmoid'), ('Softmax', 'Softmax')], validators=[InputRequired()], coerce=str)

    choice_epochs = [(el, el) for el in range(100, 3100, 100)]
    epochs = SelectField('Epochs', choices=choice_epochs, validators=[InputRequired()], coerce=int)

    choice_alpha = [(el, "{0:.3f}".format(el)) for el in np.arange(0.000, 1.0, 0.003)]
    choice_alpha[0] = (0.001, '0.001')
    alpha = SelectField('Learning rate', choices=choice_alpha,
                        validators=[InputRequired()], coerce=float)

    submit = SubmitField('Submit')

    # def __init__(self, *args, **kwargs):
    #    print(args[0])
    #    print(kwargs)
    #    super(ParameterForm, self).__init__(*args, **kwargs)
    #    if not isinstance(args[0], list):
    #        args[0] = [args[0]]
    #    self.target_col.choices = args[0]


class SimpleUpload(FlaskForm):
    file = FileField('Choose the file', [required()])
    extension = SelectField('File type', choices=[('excel', 'excel'), ('csv', 'csv')],
                            validators=[InputRequired()], coerce=str)
    index_col = SelectField('Index column', choices=[('None', 'None'), ('0', 'First'), ('-1', 'Last')],
                            validators=[InputRequired()], coerce=str)
    header = SelectField('Header', choices=[('None', 'None'), ('0', '0'), ('1', '1'), ('2', '2'), ('3', '3')],
                         validators=[InputRequired()], coerce=str)
    submit = SubmitField('Submit')


class UploadForm(SimpleUpload):
    n_layers = SelectField('Number of layers', choices=[(el, el) for el in range(0, 10)],
                           validators=[InputRequired()], coerce=int)

