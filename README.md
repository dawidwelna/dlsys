===============================
DLsystem
===============================

Deep Learning Tools for creating models, that accurately predict/recognize items belonging to the same class


Quickstart
----------

Run the following commands to bootstrap your environment ::

    git clone https://github.com/dawidwelna/DLsystem
    cd DLsystem
    pip install -r requirements/dev.txt
    
Very important before running "npm install"
Make sure to have nodejs >= 6.1.0

    curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
    sudo apt-get install -y nodejs
    
Then you can install node_modules
    
    cp .env.example .env
    npm install
    npm run-script build
    npm start  # run the webpack dev server and flask server using concurrently

You will see a pretty welcome screen.

Once you have installed your DBMS, run the following to create your app's
database tables and perform the initial migration ::

    flask db init
    flask db migrate
    flask db upgrade
    npm start


Deployment
----------

To deploy::

    export FLASK_ENV=production
    export FLASK_DEBUG=0
    export DATABASE_URL="<YOUR DATABASE URL>"
    npm run build   # build assets with webpack
    flask run       # start the flask server


LATEST!
On windows(on linux "export" instead of"set"):

    1) open Terminal and type:
        * set FLASK_APP=run.py
        * set FLASK_ENV=production

    2) create database in postgres.
    3) connect db to application in settings.py->ProdConfig
        * SQLALCHEMY_DATABASE_URI = 'postgresql://username:password@localhost/db_name'
            (username, password, db_name from postgresql)
        * in my case:
            SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:admin@localhost/DLsystem_db'

    4) go back to terminal and type
        * flask shell
    5) inside shell
        * from DLsystem.extensions import db
    6) db.create_all() <-- this will create relations in the database you provided that were defined in models
    7) you can quit() the shell



In your production environment, make sure the ``FLASK_DEBUG`` environment
variable is unset or is set to ``0``.


Shell
-----

To open the interactive shell, run ::

    flask shell

By default, you will have access to the flask ``app``.


Running Tests
-------------

To run all tests, run ::

    flask test
    
Checking Coverage
--------------

    coverage run -m pytest
    coverage report
    coverage html

Migrations
----------

Whenever a database migration needs to be made. Run the following commands ::

    flask db migrate

This will generate a new migration script. Then run ::

    flask db upgrade

To apply the migration.

For a full migration command reference, run ``flask db --help``.


Asset Management
----------------

Files placed inside the ``assets`` directory and its subdirectories
(excluding ``js`` and ``css``) will be copied by webpack's
``file-loader`` into the ``static/build`` directory, with hashes of
their contents appended to their names.  For instance, if you have the
file ``assets/img/favicon.ico``, this will get copied into something
like
``static/build/img/favicon.fec40b1d14528bf9179da3b6b78079ad.ico``.
You can then put this line into your header::

    <link rel="shortcut icon" href="{{asset_url_for('img/favicon.ico') }}">

to refer to it inside your HTML page.  If all of your static files are
managed this way, then their filenames will change whenever their
contents do, and you can ask Flask to tell web browsers that they
should cache all your assets forever by including the following line
in your ``settings.py``::

    SEND_FILE_MAX_AGE_DEFAULT = 31556926  # one year
