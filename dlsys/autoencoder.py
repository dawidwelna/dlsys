import pandas as pd
import numpy as np
from keras.models import Sequential
from keras import backend as K
from keras.layers import Dense, Activation, Embedding, Flatten
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


class AutoEncoder:
    def __init__(self, data, test_size, encode_factor, target):
        scaler = StandardScaler()
        self.target = target
        values = list(data.columns.values)
        popid = [id for id in range(0, len(values)) if values[id] == target][0]
        values.pop(popid)

        data[values] = scaler.fit_transform(
            data[values])

        self.train_x, self.test_x, self.train_y, self.test_y = train_test_split(
            data[values], data[target], test_size=test_size/100,
            random_state=1)

        model = Sequential()
        model.add(Dense(input_dim=self.train_x.shape[1], output_dim=12))
        model.add(Activation('relu'))
        model.add(Dense(output_dim=int(len(values)/encode_factor)))
        model.add(Activation('relu'))
        model.add(Dense(input_dim=self.train_x.shape[1], output_dim=12))
        model.add(Activation('relu'))
        model.add(Dense(output_dim=len(values)))
        model.add(Activation('relu'))
        self.model = model

    def compile(self, optimizer="rmsprop", loss="mean_squared_error"):
        self.model.compile(optimizer='rmsprop', loss='mean_squared_error')

    def fit(self, nb_epoch=250):
        history_callback = self.model.fit(self.train_x, self.train_x, nb_epoch=nb_epoch, verbose=2)
        self.loss_history = history_callback.history["loss"]

    def get_history_callback(self):
        output = 'Training the model...<br />'
        for i in range(0, len(self.loss_history)):
            output += 'Epoch {} | Loss: {} <br />'.format(i+1, self.loss_history[i])
        return output

    def get_summary(self):
        basedir = "/home/djwelna/PycharmProjects/DLsystem/dlsys/reports/"
        with open(basedir + 'report.txt', 'w+') as fh:
            # Pass the file handle in as a lambda function to make it callable
            self.model.summary(print_fn=lambda x: fh.write(x + '<br>'))

        with open(basedir+'report.txt', 'r+') as f:
            summary = f.readlines()

        return summary

    def get_encoded_df(self):
        hidden_op = get_activations(model=self.model, layer=2, X_batch=self.train_x)
        n_cols = hidden_op[0].shape[1]

        df = pd.DataFrame()
        dct = {}
        for i in range(0, n_cols):
            dct[f'column{i}'] = hidden_op[0][:, i]

        dct[self.target] = self.train_y

        df = pd.DataFrame(dct)

        return df


def get_activations(model, layer, X_batch):
    get_activations = K.function([model.layers[0].input, K.learning_phase()], [model.layers[layer].output, ])
    activations = get_activations([X_batch, 0])
    return activations
