import numpy as np
import pandas as pd
from dlsys import dlsystem as dl


class NeuralNet(object):
    def __init__(self, data_set, target_col, ):
        """
        Net initialization
        :param data_set: pandas dataframe
        :param target_col: the column that is the label/identifier for data
        """
        self.output_neurons = len(data_set[target_col].unique())
        print(self.output_neurons)
        data_set = pd.get_dummies(data_set, columns=[target_col])  # One Hot Encoding
        values = list(data_set.columns.values)

        y = data_set[values[-self.output_neurons:]]
        print(y)
        self.y = np.array(y, dtype='float32')
        X = data_set[values[:-self.output_neurons]]
        print(X)
        self.X = np.array(X, dtype='float32')

    def train_test_shuffle(self, test_size, seed):
        """
        shuffle data, divide data between test and train parts
        :param test_size: float, the percentage of dataset.
        :param seed: integer
        """
        # Shuffle Data

        test_perc = int(self.X.shape[0]*test_size/100)

        np.random.seed(seed)
        indices = np.random.choice(len(self.X), len(self.X), replace=False)
        X_values = self.X[indices]
        y_values = self.y[indices]

        # Creating a Train and a Test Dataset
        self.test_size = test_perc
        self.X_test = X_values[-test_perc:]
        self.X_train = X_values[:-test_perc]
        self.y_test = y_values[-test_perc:]
        self.y_train = y_values[:-test_perc]

    def configure_net(self, layers_config, activ_func):
        """
        configures the nets, connects all layers
        :param layers_config: a list e.g [4, 16, 16, 3]
        :param activ_func: str e.g 'Softmax'
        """
        # Create a new graph
        dl.Graph().set_as_default()
        # Initialize Placeholers
        self.X_data = dl.Placeholder()
        self.y_target = dl.Placeholder()
        # Input neurons : 4
        # Hidden neurons : 16
        # Output neurons : 3
        self.sess = dl.Session()

        activation_fun = getattr(dl, activ_func)

        # Create variables for Neural Network layers
        W = []
        B = []
        p_output = []
        # we are setting weights and biases in a loop
        # loop as many as the number of layers
        for i in range(0, len(layers_config)-1):
            W.append(dl.Variable(np.random.randn(layers_config[i], layers_config[i+1])))
            B.append(dl.Variable(np.random.randn(layers_config[i+1])))
            if i == 0:
                p_output.append(activation_fun(dl.Add(dl.Matmul(self.X_data, W[i]), B[i])))
            else:
                p_output.append(activation_fun(dl.Add(dl.Matmul(p_output[i-1], W[i]), B[i])))

        self.W = W
        self.B = B
        self.p_output = p_output
        # the loop could be be simpified to the code below in the simple case
        """ 
        w1 = dl.Variable(np.random.randn(layers_config[0], hidden_layer[0]))  # Inputs -> Hidden Layer
        b1 = dl.Variable(np.random.randn(hidden_layer[0]))  # First Bias

        w2 = dl.Variable(np.random.randn(hidden_layer[0], hidden_layer[1]))
        b2 = dl.Variable(np.random.randn(hidden_layer[1]))

        w_out = dl.Variable(np.random.randn(hidden_layer[1], self.output_neurons))  # Hidden layer -> Outputs
        b_out = dl.Variable(np.random.randn(self.output_neurons))  # Second Bias

        # Operations
        hidden_output1 = dl.Softmax(dl.Add(dl.Matmul(X_data, w1), b1))
        hidden_output2 = dl.Softmax(dl.Add(dl.Matmul(hidden_output1, w2), b2))
        final_output = dl.Softmax(dl.Add(dl.Matmul(hidden_output2, w_out), b_out))"""
        # set Cost(loss) Function
        self.loss = dl.Negative(dl.ReduceSum(dl.ReduceSum(dl.Multiply(self.y_target, dl.Log(p_output[-1])), axis=1)))

    def train(self, learning_rate, epoch):
        """
        training the model with gradient descent
        :param learning_rate: float, (alpha parameter) how fast the gradient changes
        :param epoch: int, how many iterations(epochs)
        :return: str, output
        """
        # Optimizer
        optimizer = dl.GradientDescentOptimizer(learning_rate=learning_rate).minimize(self.loss)

        # Initialize variables

        interval = 50
        feed_dict = {self.X_data: self.X_train, self.y_target: self.y_train}
        # Training
        print('Training the model...')
        output = 'Training the model...<br />'
        for i in range(1, (epoch + 1)):
            J_value = self.sess.run(self.loss, feed_dict)
            if i % interval == 0:
                print('Epoch', i, '|', 'Loss:', J_value)
                output += 'Epoch {} | Loss: {} <br />'.format(i, J_value)
            self.sess.run(optimizer, feed_dict)
        return output

    def test_model(self):
        """
        testing the model with test set, how accurate the model became
        :return: str, output
        """
        # Prediction
        print('Testing the model...')
        output = '<br />Testing the model...<br />'
        out = np.zeros((len(self.X_test), self.output_neurons))
        count = 0
        for i in range(len(self.X_test)):
            out[i] = np.rint(self.sess.run(self.p_output[-1], feed_dict={self.X_data: [self.X_test[i]]}))
            print('Actual:', self.y_test[i], 'Predicted:',
                  np.rint(self.sess.run(self.p_output[-1], feed_dict={self.X_data: [self.X_test[i]]})))
            output += 'Actual:' + str(self.y_test[i]) + 'Predicted:' + \
                      str(np.rint(self.sess.run(self.p_output[-1], feed_dict={self.X_data: [self.X_test[i]]}))) + '<br />'
            if all(self.y_test[i] == out[i]):
                count += 1

        accuracy = count/self.test_size*100
        print("Accuracy is: {}/{} ({})".format(count, self.test_size, accuracy))
        output += "Accuracy is: {}/{} ({})".format(count, self.test_size, accuracy)
        return output
